/*
 * Simple caching library with expiration capabilities
 *     Copyright (c) 2012, Radu Ioan Fericean
 *                   2013-2017, Christian Muehlhaeuser <muesli@gmail.com>
 *
 *   For license see LICENSE.txt
 */

package cache2go

// Cache returns the existing cache table with given name or creates a new one
// if the table does not exist yet.
func Cache() *CacheTable {
	t := &CacheTable{
		items: make(map[any]*CacheItem),
	}

	return t
}

// CacheOf returns the existing cache table with given name or creates a new one
// if the table does not exist yet.
func CacheOf[K comparable, V any]() *CacheTableOf[K, V] {
	t := &CacheTableOf[K, V]{
		items: make(map[K]*CacheItemOf[K, V]),
	}

	return t
}
